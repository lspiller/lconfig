set visualbell t_vb=sset visualbell t_vb=et nocompatible
filetype off
execute pathogen#infect()
call pathogen#helptags()
call pathogen#runtime_append_all_bundles()

" Don't mess up local directories
set dir=~/.vimcrud
set backupdir=~/.vimcrud

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

"" Reduce autocomplete
set complete=.,w,b

" Enable syntax highlighting
syntax on

"set cursorline
"set cursorcolum
set hidden
set autowrite
set visualbell
set noerrorbells
set wildmenu
set wildmode=list:longest
set wildignore=*.o,*.root,*.pyc,*.png,*.pdf,*.ps
set ignorecase
set smartcase
set scrolloff=3
set novisualbell
set nojoinspaces

" Enable filetype detection
filetype plugin indent on

set history=700
set undolevels=700
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching
set autoindent		" always set autoindenting on

set smartindent

" Real programmers don't use TABs but spaces
"set tabstop=4
set softtabstop=4
set shiftwidth=4
"set shiftround
"set expandtab

" Makefile sanity
autocmd BufEnter ?akefile* set noet ts=8 sw=8

" reStructuredText
autocmd BufEnter *.rst set et ts=3 sw=3


" Majority vote on tabs vs spaces
"function! Kees_settabs()
"    if len(filter(getbufline(winbufnr(0), 1, "$"), 'v:val =~ "^\\t"')) > len(filter(getbufline(winbufnr(0), 1, "$"), 'v:val =~ "^ "'))
"        set noet ts=8 sw=8
"    endif
"endfunction
"autocmd BufReadPost * call Kees_settabs()

" For all text files set 'textwidth' to 78 characters.
"autocmd FileType text setlocal textwidth=78

" Delete trailing whitespace before saving in tex, cpp and python
"autocmd BufWritePre *.cxx,*.cpp,*.icc,*.cc,*.h,*.py,*.tex,*.rst,*.md,*.bib :%s/\s\+$//e

" When editing a file, always jump to the last known cursor position.
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" NERD Tree settings
map <silent> <C-P> <ESC>:NERDTreeToggle<CR>
let NERDTreeIgnore=['CVS', 'pyc$', '\.root$', 'pdf$', 'png$', '@Batch', 'xml.bak$', 'xml.fragment$']
let NERDTreeWinSize=61
let NERDTreeWinPos=0
let NERDTreeChDirMode=2 "always set root as cwd
let NERDTreeChristmasTree = 1

let g:syntastic_auto_loc_list=1
set statusline=%t\ %m%r[%04l,%02c]
set statusline+=\ \ %#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
set laststatus=2
set noerrorbells
set vb t_vb=
set nospell

" Don't use plaintex, but tex
let g:tex_flavor='latex'

hi StatColor guibg=#95e454 guifg=black ctermbg=lightgreen ctermfg=black
hi Modified guibg=orange guifg=black ctermbg=lightred ctermfg=black

function! MyStatusLine(mode)
    let statusline=""
    if a:mode == 'Enter'
        let statusline.="%#StatColor#"
    endif
    let statusline.="\(%n\)\ %f\ "
    if a:mode == 'Enter'
        let statusline.="%*"
    endif
    let statusline.="%#Modified#%m"
    if a:mode == 'Leave'
        let statusline.="%*%r"
    elseif a:mode == 'Enter'
        let statusline.="%r%*"
    endif
    let statusline .= "\ (%l/%L,\ %c)\ %P%=%h%w\ %{fugitive#statusline()} %y\ [%{&encoding}:%{&fileformat}]\ \ "
    return statusline
endfunction

"au WinEnter * setlocal statusline=%!MyStatusLine('Enter')
"au WinLeave * setlocal statusline=%!MyStatusLine('Leave')
set statusline=%!MyStatusLine('Enter')

function! InsertStatuslineColor(mode)
  if a:mode == 'i'
    hi StatColor guibg=orange ctermbg=lightred
  elseif a:mode == 'r'
    hi StatColor guibg=#e454ba ctermbg=magenta
  elseif a:mode == 'v'
    hi StatColor guibg=#e454ba ctermbg=magenta
  else
    hi StatColor guibg=red ctermbg=red
  endif
endfunction 

au InsertEnter * call InsertStatuslineColor(v:insertmode)
au InsertLeave * hi StatColor guibg=#95e454 guifg=black ctermbg=lightgreen ctermfg=black

"command! CondenseBlanks :%s/\n\{3,}/\r\r/e
"let $uw='/afs/hep.wisc.edu/home/efriis'

func! WordProcessorMode() 
  setlocal formatoptions=1 
  setlocal noexpandtab 
  map j gj 
  map k gk 
  setlocal smartindent 
  setlocal spell spelllang=en_us 
  setlocal wrap 
  setlocal linebreak 
  setlocal syntax=none 
endfu 
com! WP call WordProcessorMode()

if (v:version >= 700)
    highlight SpellBad      ctermfg=Red         term=Reverse        guisp=Red       gui=undercurl   ctermbg=White
    highlight SpellCap      ctermfg=Green       term=Reverse        guisp=Green     gui=undercurl   ctermbg=White
    highlight SpellLocal    ctermfg=Cyan        term=Underline      guisp=Cyan      gui=undercurl   ctermbg=White
    highlight SpellRare     ctermfg=Magenta     term=underline      guisp=Magenta   gui=undercurl   ctermbg=White
endif " version 7+ 

" http://stackoverflow.com/questions/235439/vim-80-column-layout-concerns
"autocmd BufEnter *.py,*.cpp,*.cxx,*.rst,*.tex highlight OverLength ctermbg=darkred ctermfg=white guibg=#FFD9D9
"autocmd BufEnter *.py,*.cpp,*.cxx,*.rst,*.tex match OverLength /\%80v.\+/
"
"autocmd BufEnter *.py,*.cpp,*.cxx,*.rst,*.tex
"            \ if exists("&colorcolumn") |
"                \ set colorcolumn=80 |
"            \ endif
"augroup END

function! s:Underline(chars)
  let chars = empty(a:chars) ? '-' : a:chars
  let nr_columns = virtcol('$') - 1
  let uline = repeat(chars, (nr_columns / len(chars)) + 1)
  put =strpart(uline, 0, nr_columns)
endfunction
command! -nargs=? Underline call s:Underline(<q-args>)

" http://stackoverflow.com/questions/2360249/vim-automatically-removes-indentation-on-python-comments
inoremap # X<BS>#

" https://github.com/mbrochh/vim-as-a-python-ide/blob/master/.vimrc
" http://www.youtube.com/watch?feature=endscreen&NR=1&v=YhqsjUUHj6g
"set mouse=a
set bs=2
" Autromatic reloading of .vimrc
autocmd! bufwritepost .vimrc source %
autocmd! bufwritepost vimrc source %

" better copy & paste
set pastetoggle=<F2>
set clipboard=unnamed

" remove highlight of your last search
noremap <C-n> :nohl<CR>
vnoremap <C-n> :nohl<CR>
inoremap <C-n> :nohl<CR>

" quicksave
noremap <C-Z> :update<CR>
inoremap <C-Z> <C-C>:update<CR>
vnoremap <C-Z> <C-O>:update<CR>

" rebind <Leader> key
let mapleader = ","

" quickexit
noremap <Leader>e :quit<CR>  " quit current window
noremap <Leader>E :qa!<CR>   " quit all windows

" bind keys to move around windows
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" easier moving between tabs
map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>
map <Leader>b <esc>:tabnew<CR>

" map sort function to a key
vnoremap <Leader>s :sort<CR>

" easier moving of code blocks
vnoremap < <gv
vnoremap > >gv

" easier formatting of paragraphs
vmap Q gq
nmap Q gqap

" Make search case insensitive
set hlsearch

" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
set nobackup
set nowritebackup
set noswapfile

" Showing line numbers and length
"set number  " show line numbers
"set tw=79   " width of document (used by gd)
"set nowrap  " don't automatically wrap on load
"set fo-=t   " don't automatically wrap text when typing

"if version >= 703
"    set colorcolumn=80
"    highlight ColorColumn ctermbg=233
"endif

" auto linebreak in text
"au BufEnter *.txt *.tex setl tx ts=4 sw=4 fo+=n2a

" set nofoldenable    " disable folding

"au BufRead,BufNewFile *.tex,*.sty set fileformat=unix

" Laurence's additions
"Use \l to toggle line numbers on and off
nmap \n :setlocal number!<CR>

"Use \o to enable paste mode
nmap \o :setlocal paste!<CR>

"Make j/down and k/up move down/up one row instead of one line! Useful when one line spans multiplie rows.
nmap j gj
nmap k gk
nmap <Down> gj
nmap <Up> gk

"Map \c to clear the highlighting from hlsearch
nmap \c :nohlsearch<CR>

nmap \s :set spell!<CR>


"Activates tab completion for commands
set wildmenu
set wildmode=list:longest,full

"When a line exceeds the length of the screen and is continued on the next line,
"displacs a character to show the line has been continued
let &showbreak = '...'

set wrap

"Open URL at current line in a browser.
function! Browser()
	let line = getline (".")
	let line = matchstr(line, "http[^   ]*")
	exec "!lynx ".line
endfunction

"set tm=500


"Fix backspace. ie., can backspace across lines and indents.
set backspace=eol,start,indent
set whichwrap+=<,>,h,l


"Add to clipboard using ctrl-y ctrl-p
nnoremap <C-y> "+y
vnoremap <C-y> "+y
nnoremap <C-p> "+gP
vnoremap <C-p> "+gP

"command Fbr :normal i\#-------------------------------------------------------------------------<ESC>

" Return to last edit position when opening files
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif


" Delete trailing white space on save, useful for Python and CoffeeScript ;)
"func! DeleteTrailingWS()
"   exe "normal mz"
"   %s/\s\+$//ge
"   exe "normal `z"
"endfunc
"autocmd BufWrite *.py :call DeleteTrailingWS()
"autocmd BufWrite *.coffee :call DeleteTrailingWS()

" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>


if &term =~ "xterm"
  let &t_SI .= "\e[?2004h"
  let &t_EI .= "\e[?2004l"
  let &pastetoggle = "\e[201~"
  function XTermPasteBegin(ret)
     set paste
     return a:ret
  endfunction
  inoremap <special> <expr> <Esc>[200~ XTermPasteBegin("")
endif

"
"colorscheme desert

set shortmess=atI
set mat=5

autocmd FileType python set omnifunc=pythoncomplete#Complete

set expandtab
set tabstop=8
set softtabstop=4
set shiftwidth=4
set autoindent

" Keep search term centred
nnoremap <silent> n nzz
nnoremap <silent> N Nzz
nnoremap <silent> * *zz
nnoremap <silent> # #zz
nnoremap <silent> g* g*zz
nnoremap <silent> g# g#zz

"Clear search
noremap <silent><Leader>/ :nohls<CR>

"Keep slab highlighted when using indent
vnoremap < <gv
vnoremap > >gv

"Show git diff when commiting in a split window.
autocmd FileType gitcommit DiffGitCached | wincmd p

"*.md read as markdown files instead of Modula-2
autocmd BufNewFile,BufReadPost *.md set filetype=markdown

"*.json highlighting. Uses javascript, but generally good enough
autocmd BufNewFile,BufRead *.json set ft=javascript

" Pytest
nmap <silent><Leader>f <Esc>:Pytest file<CR>
nmap <silent><Leader>c <Esc>:Pytest class<CR>
nmap <silent><Leader>m <Esc>:Pytest method<CR>
nmap <silent><Leader>f <Esc>:Pytest file --pdb<CR>
nmap <silent><Leader>b <Esc>
map <silent><Leader>b oimport pdb; pdb.set_trace()<Esc>
" TagList
nnoremap <silent> <F8> :TlistToggle<CR>

"Ropevim
"Omnicomplete
autocmd FileType python setlocal omnifunc=RopeCompleteFunc

"add the name of modules you want to autoimport
let g:ropevim_autoimport_modules = ["os", "shutil", "ROOT"]

"Enable ropevim autocompletions
let ropevim_vim_completion=1
autocmd FileType python setlocal omnifunc=RopeCompleteFunc

"Disable quickfix window for pyflakes
let g:pyflakes_use_quickfix = 0

" Don't break words when linewrapping
set linebreak

" No splash screen
set shortmess+=I

" No blinking screen/visual bell
set visualbell t_vb=

" ZZ in normal mode to :wq and ZQ to :q!

" Placement of new windows and splits
set splitbelow
set splitright

" Pyflakes - press F7 to use
" Show markers in file
let g:flake8_show_in_file=1
let g:flake8_show_in_gutter=1
" Rmove markers in file
autocmd FileType python map <buffer> <F6> :call flake8#Flake8UnplaceMarkers()<CR>
let g:flake8_quickfix_location="bottomleft"
let g:flake8_quickfix_height=4
let g:flake8_show_quickfix=0  " don't show quickfix window by default

"flake8_error_marker='EE'     " set error marker to 'EE'
"flake8_warning_marker='WW'   " set warning marker to 'WW'
"flake8_pyflake_marker=''     " disable PyFlakes warnings
"flake8_complexity_marker=''  " disable McCabe complexity warnings
"flake8_naming_marker=''      " disable naming warnings
"To customize the colors used for markers, define the highlight groups, Flake8_Error, Flake8_Warning, Flake8_PyFlake, Flake8_Complexity, Flake8_Naming:

" to use colors defined in the colorscheme
"highlight link Flake8_Error      Error
"highlight link Flake8_Warning    WarningMsg
"highlight link Flake8_Complexity WarningMsg
"highlight link Flake8_Naming     WarningMsg
"highlight link Flake8_PyFlake    WarningMsg
