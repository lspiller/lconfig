#!/bin/bash

cd ~/
mkdir .oldconfigs
mv .bashrc .oldconfigs/
mv .inputrc .oldconfigs/
mv .tmux.conf .oldconfigs/
mv .screenrc .oldconfigs/
mv .rootrc .oldconfigs/
mv .vimrc .oldconfigs/


ln -s .bashrc .LConfig/bashrc
ln -s .inputrc .LConfig/inputrc
ln -s .tmux.conf .LConfig/tmux.conf
ln -s .screenrc .LConfig/screenrc
ln -s .rootrc .LConfig/rootrc
ln -s .vimrc .LConfig/vimrc
