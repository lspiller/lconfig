if $TERM == "xterm-256color"
    set t_Co=256
endif

execute pathogen#infect()
call pathogen#helptags()
call pathogen#runtime_append_all_bundles()

" Map super undo to leader+u
nnoremap <leader>u :GundoToggle<CR>

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

" CtrlP settings - fuzzy file search
let g:ctrlp_match_window = 'bottom,order:ttb' "Show top to bottom
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0 "Open in a new buffer
let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""' " Runs faster when using ag

"" Reduce autocomplete
set complete=.,w,b

" Enable syntax highlighting
syntax on

" Highlight text inserted when last in insert mode
nnoremap gV `[v`]

"set cursorline
"set cursorcolum
set hidden
set autowrite
set noerrorbells
set wildmenu
set wildmode=list:longest
set wildignore=*.o,*.root,*.pyc,*.png,*.pdf,*.ps
set ignorecase
set smartcase
"set scrolloff=3
set novisualbell
set nojoinspaces

" Enable filetype detection
filetype plugin indent on

set history=700
set undolevels=700
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching
set autoindent		" always set autoindenting on

set smartindent

set expandtab
set tabstop=8
set softtabstop=4
set shiftwidth=4

" Makefile sanity
autocmd BufEnter ?akefile* set noet ts=8 sw=8

" reStructuredText
autocmd BufEnter *.rst set et ts=3 sw=3


" Majority vote on tabs vs spaces
"function! Kees_settabs()
"    if len(filter(getbufline(winbufnr(0), 1, "$"), 'v:val =~ "^\\t"')) > len(filter(getbufline(winbufnr(0), 1, "$"), 'v:val =~ "^ "'))
"        set noet ts=8 sw=8
"    endif
"endfunction
"autocmd BufReadPost * call Kees_settabs()

" For all text files set 'textwidth' to 78 characters.
"autocmd FileType text setlocal textwidth=78

" Delete trailing whitespace before saving in tex, cpp and python
autocmd BufWritePre *.cxx,*.cpp,*.icc,*.cc,*.h,*.py,*.tex,*.rst,*.md,*.bib :%s/\s\+$//e

" When editing a file, always jump to the last known cursor position.
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" NERD Tree settings
map <silent> <C-P> <ESC>:NERDTreeToggle<CR>
let NERDTreeIgnore=['CVS', 'pyc$', '\.root$', 'pdf$', 'png$', '@Batch', 'xml.bak$', 'xml.fragment$']
let NERDTreeWinSize=61
let NERDTreeWinPos=0
let NERDTreeChDirMode=2 "always set root as cwd
let NERDTreeChristmasTree = 1

set statusline=%t\ %m%r[%04l,%02c]
set statusline+=\ \ %#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
set laststatus=2
set noerrorbells
set vb t_vb=
set nospell

" Don't use plaintex, but tex
let g:tex_flavor='latex'

hi StatColor guibg=#95e454 guifg=black ctermbg=lightgreen ctermfg=black
hi Modified guibg=orange guifg=black ctermbg=lightred ctermfg=black

function! MyStatusLine(mode)
    let statusline=""
    if a:mode == 'Enter'
        let statusline.="%#StatColor#"
    endif
    let statusline.="\(%n\)\ %f\ "
    if a:mode == 'Enter'
        let statusline.="%*"
    endif
    let statusline.="%#Modified#%m"
    if a:mode == 'Leave'
        let statusline.="%*%r"
    elseif a:mode == 'Enter'
        let statusline.="%r%*"
    endif
    let statusline .= "\ (%l/%L,\ %c)\ %P%=%h%w\ %{fugitive#statusline()} %y\ [%{&encoding}:%{&fileformat}]\ \ "
    return statusline
endfunction

"au WinEnter * setlocal statusline=%!MyStatusLine('Enter')
"au WinLeave * setlocal statusline=%!MyStatusLine('Leave')
set statusline=%!MyStatusLine('Enter')

function! InsertStatuslineColor(mode)
  if a:mode == 'i'
    hi StatColor guibg=orange ctermbg=lightred
  elseif a:mode == 'r'
    hi StatColor guibg=#e454ba ctermbg=magenta
  elseif a:mode == 'v'
    hi StatColor guibg=#e454ba ctermbg=magenta
  else
    hi StatColor guibg=red ctermbg=red
  endif
endfunction

au InsertEnter * call InsertStatuslineColor(v:insertmode)
au InsertLeave * hi StatColor guibg=#95e454 guifg=black ctermbg=lightgreen ctermfg=black

if (v:version >= 700)
    highlight SpellBad      ctermfg=Red         term=Reverse        guisp=Red       gui=undercurl   ctermbg=White
    highlight SpellCap      ctermfg=Green       term=Reverse        guisp=Green     gui=undercurl   ctermbg=White
    highlight SpellLocal    ctermfg=Cyan        term=Underline      guisp=Cyan      gui=undercurl   ctermbg=White
    highlight SpellRare     ctermfg=Magenta     term=underline      guisp=Magenta   gui=undercurl   ctermbg=White
endif " version 7+

" http://stackoverflow.com/questions/235439/vim-80-column-layout-concerns
"autocmd BufEnter *.py,*.cpp,*.cxx,*.rst,*.tex highlight OverLength ctermbg=darkred ctermfg=white guibg=#FFD9D9
"autocmd BufEnter *.py,*.cpp,*.cxx,*.rst,*.tex match OverLength /\%80v.\+/
"
"autocmd BufEnter *.py,*.cpp,*.cxx,*.rst,*.tex
"            \ if exists("&colorcolumn") |
"                \ set colorcolumn=80 |
"            \ endif
"augroup END

" http://stackoverflow.com/questions/2360249/vim-automatically-removes-indentation-on-python-comments
inoremap # X<BS>#

" https://github.com/mbrochh/vim-as-a-python-ide/blob/master/.vimrc
" http://www.youtube.com/watch?feature=endscreen&NR=1&v=YhqsjUUHj6g
"set mouse=a
set bs=2
" Automatic reloading of .vimrc
autocmd! bufwritepost .vimrc source %
autocmd! bufwritepost vimrc source %

" better copy & paste
set pastetoggle=<F2>
set clipboard=unnamed

" rebind <Leader> key
"let mapleader = ","

" quickexit
noremap <Leader>e :quit<CR>  " quit current window
noremap <Leader>E :qa!<CR>   " quit all windows

" bind keys to move around windows
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" easier moving between tabs
map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>
map <Leader>b <esc>:tabnew<CR>

" easier moving of code blocks
vnoremap < <gv
vnoremap > >gv

" Make search case insensitive
set hlsearch

" Disable stupid backup and swap files - they trigger too many events
" for file system watchers
set nobackup
set nowritebackup
set noswapfile

"Use \l to toggle line numbers on and off
nmap \l :setlocal number!<CR>

"Make j/down and k/up move down/up one row instead of one line! Useful when one line spans multiplie rows.
nmap j gj
nmap k gk
nmap <Down> gj
nmap <Up> gk

"Map \c to clear the highlighting from hlsearch
nmap <Leader>c :nohlsearch<CR>

"Spelling
nmap <Leader>s :set spell!<CR>

"When a line exceeds the length of the screen and is continued on the next line,
"displacs a character to show the line has been continued
let &showbreak = '...'

set wrap

"Fix backspace. ie., can backspace across lines and indents.
set backspace=eol,start,indent
set whichwrap+=<,>,h,l


"Add to clipboard using ctrl-y ctrl-p
nnoremap <C-y> "+y
vnoremap <C-y> "+y
nnoremap <C-p> "+gP
vnoremap <C-p> "+gP

" Return to last edit position when opening files
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" Delete trailing white space on save, useful for Python
func! DeleteTrailingWS()
   exe "normal mz"
   %s/\s\+$//ge
   exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()

" Reduce 'press enter to continue' prompts
set shortmess=atI
set mat=5


" Keep search term centred
nnoremap <silent> n nzz
nnoremap <silent> N Nzz
nnoremap <silent> * *zz
nnoremap <silent> # #zz
nnoremap <silent> g* g*zz
nnoremap <silent> g# g#zz

"Show git diff when commiting in a split window.
autocmd FileType gitcommit DiffGitCached | wincmd p

"*.md read as markdown files instead of Modula-2
autocmd BufNewFile,BufReadPost *.md set filetype=markdown

"*.json highlighting. Uses javascript, but generally good enough
autocmd BufNewFile,BufRead *.json set ft=javascript

" Don't break words when linewrapping
set linebreak

" No blinking screen/visual bell
set visualbell t_vb=

" ZZ in normal mode to :wq and ZQ to :q!

" Placement of new windows and splits
set splitbelow
set splitright

"*cterm-colors*
"NR-16   NR-8    COLOR NAME
"0       0       Black
"1       4       DarkBlue
"2       2       DarkGreen
"3       6       DarkCyan
"4       1       DarkRed
"5       5       DarkMagenta
"6       3       Brown, DarkYellow
"7       7       LightGray, LightGrey, Gray, Grey
"8       0*      DarkGray, DarkGrey
"9       4*      Blue, LightBlue
"10      2*      Green, LightGreen
"11      6*      Cyan, LightCyan
"12      1*      Red, LightRed
"13      5*      Magenta, LightMagenta
"14      3*      Yellow, LightYellow
"15      7*      White
"highlight Comment ctermfg=darkgray
"highlight Constant ctermfg=lightgray
"highlight Constant ctermfg=214
colorscheme inkpot

" Vim UI
set lsp=0 " space it out a little more (easier to read)
set lz " do not redraw while running macros (much faster) (LazyRedraw)
set report=0 " tell us when anything is changed via :...
set noerrorbells " don't make noise

iab xdate <c-r>=strftime("%d/%m/%y %H:%M:%S")<cr>
autocmd FileType c :set cindent

set nofoldenable    " disable folding
